﻿using ControleHoras.Helper;
using Firebase.Xamarin.Auth;
using Firebase.Xamarin.Database;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ControleHoras.BancoDadosFirebase
{
    public class FirebaseBanco
    {

        public FirebaseClient fbCliente;
        public FirebaseAuth firebaseAuth;
        public FirebaseAuthProvider firebaseProvider;
        const string CONFIG_KEY = "AIzaSyCHtqp7jSxDbeSShUAwreqoME4GPCfk7Zs";

        public FirebaseBanco()
        {
            fbCliente = new FirebaseClient("https://controledeponto-4305f.firebaseio.com/");
            firebaseAuth = new FirebaseAuth();
            firebaseProvider = new FirebaseAuthProvider(new FirebaseConfig(CONFIG_KEY));
        }

        public async Task autenticacaoUsuario(string nome, string password)
        {
            var user = await App.firebaseAuthProvider.SignInWithEmailAndPasswordAsync(nome, password);
            UsuarioLocal.userToken = user.FirebaseToken;
            UsuarioLocal.userEmail = user.User.Email;
            UsuarioLocal.userUID = user.User.LocalId;
        }

    }
}
