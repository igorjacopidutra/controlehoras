﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace ControleHoras.Modelos
{
    [Table("Local")]
    public class Local
    {
        [PrimaryKey, AutoIncrement]
        public int IIdentificado { get; set; }
        public string SDescricao { get; set; }
        public string SRazaoSocial { get; set; }
        public string SNomeFantasia { get; set; }
        public int ITelefone { get; set; }
        public int ICelular { get; set; }
        public string SEmail { get; set; }
        public int ICnpj { get; set; }
        public int SSite { get; set; }

    }
}
