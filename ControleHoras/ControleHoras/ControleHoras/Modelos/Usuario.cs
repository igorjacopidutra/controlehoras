﻿using System;
using System.Collections.Generic;
using System.Text;
using ControleHoras.Modelos;
using SQLite;

namespace ControleHoras.Modelos
{
    [Table("Usuario")]
    public class Usuario
    {
        [PrimaryKey, AutoIncrement]
        public int IIdentificado { get; set; }
        public string SEmail { get; set; }
        public string SSenha { get; set; }
        public string SSexo { get; set; }
        public string SPrimeiroNome { get; set; }
        public string SSegundoNome { get; set; }
        public int ITelefone { get; set; }
        public int ICelular { get; set; }
        public string SCargo { get; set; }
        public Endereco EndEndereco { get; set; }
        public Local LocLocal { get; set; }
    }
}
