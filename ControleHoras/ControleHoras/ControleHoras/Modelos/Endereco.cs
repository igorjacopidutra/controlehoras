﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace ControleHoras.Modelos
{
    [Table("Endereco")]
    public class Endereco
    {   
        [PrimaryKey, AutoIncrement]
        public int IIdentificado { get; set; }
        public string SNumero { get; set; }
        public string SEndereco { get; set; }
        public string SSexo { get; set; }
        public string SBairro { get; set; }
        public string SCidade { get; set; }
        public string SEstado { get; set; }
        public string SPais { get; set; }

    }
}
