﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;
using ControleHoras.Modelos;
using Xamarin.Forms;

namespace ControleHoras.BancoSQLLite

{
    class BancodeDados
    {
        private SQLiteConnection _conexao;

        public BancodeDados() 

        {
            var dep = DependencyService.Get<ICaminho>();
            string caminho = dep.ObterCaminho("database.sqlite");

            _conexao = new SQLiteConnection(caminho);
            _conexao.CreateTable<Usuario>();
            _conexao.CreateTable<Local>();
            _conexao.CreateTable<Endereco>();
        }

        // -------------------------------------- USUARIO ------------------------------------
        public List<Usuario> ConsultarUsuario()
        {
            return _conexao.Table<Usuario>().ToList();
        }
        public List<Usuario> PesquisarUsuario(string palavra)
        {
            return _conexao.Table<Usuario>().Where(a => a.SPrimeiroNome.Contains(palavra)).ToList();
        }
        public Usuario ObterVagaPorIdUsuario(int id)
        {
            return _conexao.Table<Usuario>().Where(a => a.IIdentificado == id).FirstOrDefault();
        }
        public void Cadastro(Usuario usuario)
        {
            _conexao.Insert(usuario);
        }
        public void AtualizacaoUsuario(Usuario usuario)
        {
            _conexao.Update(usuario);
        }
        public void ExclusaoUsuario(Usuario usuario)
        {
            _conexao.Delete(usuario);
        }
        public void ExcluirTudoUsuario()
        {
            _conexao.DeleteAll<Usuario>();
        }
        public Usuario ObterVagaPorEmailUsuario(String Email)
        {
            return _conexao.Table<Usuario>().Where(a => a.SEmail == Email).FirstOrDefault();
        }

        // -------------------------------------- LOCAL -------------------------------------

        public List<Local> ConsultarLocal()
        {
            return _conexao.Table<Local>().ToList();
        }
        public List<Local> PesquisarLocal(string palavra)
        {
            return _conexao.Table<Local>().Where(a => a.SDescricao.Contains(palavra)).ToList();
        }
        public Local ObterVagaPorIdLocal(int id)
        {
            return _conexao.Table<Local>().Where(a => a.IIdentificado == id).FirstOrDefault();
        }
        public void CadastroLocal(Local local)
        {
            _conexao.Insert(local);
        }
        public void AtualizacaoLocal(Local local)
        {
            _conexao.Update(local);
        }
        public void ExclusaoLocal(Local local)
        {
            _conexao.Delete(local);
        }
        public void ExcluirTudoLocal()
        {
            _conexao.DeleteAll<Local>();
        }
        // ------------------------------------- ENDERECO -----------------------------------
        public List<Endereco> ConsultarEndereco()
        {
            return _conexao.Table<Endereco>().ToList();
        }
        public List<Endereco> PesquisarUsuarioEndereco(string palavra)
        {
            return _conexao.Table<Endereco>().Where(a => a.SEndereco.Contains(palavra)).ToList();
        }
        public Endereco ObterVagaPorIdUsuarioEndereco(int id)
        {
            return _conexao.Table<Endereco>().Where(a => a.IIdentificado == id).FirstOrDefault();
        }
        public void CadastroEndereco(Endereco endereco)
        {
            _conexao.Insert(endereco);
        }
        public void AtualizacaoUsuarioEndereco(Endereco endereco)
        {
            _conexao.Update(endereco);
        }
        public void ExclusaoEndereco(Endereco endereco)
        {
            _conexao.Delete(endereco);
        }
        public void ExcluirTudoEndereco()
        {
            _conexao.DeleteAll<Endereco>();
        }
    }
}
