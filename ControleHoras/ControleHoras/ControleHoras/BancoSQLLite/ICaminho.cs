﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ControleHoras.BancoSQLLite
{
    public interface ICaminho
    {
        string ObterCaminho(string NomeArquivoBanco);
    }
}
