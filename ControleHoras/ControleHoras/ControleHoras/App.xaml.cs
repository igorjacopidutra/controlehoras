﻿using Firebase.Xamarin.Auth;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace ControleHoras
{
    public partial class App : Application
    {
        const string CONFIG_KEY = "AIzaSyCHtqp7jSxDbeSShUAwreqoME4GPCfk7Zs";
        public static FirebaseAuthProvider firebaseAuthProvider = new FirebaseAuthProvider(new FirebaseConfig(CONFIG_KEY));

        public App()
        {
            InitializeComponent();

            MainPage = new MainPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
