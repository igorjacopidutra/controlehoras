﻿using ControleHoras.BancoDadosFirebase;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using System;
using System.Threading.Tasks;

namespace ControleHoras.Paginas
{
    public partial class MyPopupPage : PopupPage
    {
        FirebaseBanco servico;

        public MyPopupPage ()
		{
			InitializeComponent ();
            servico = new FirebaseBanco();
        }

        async void clkGoPrincipal(object sender, EventArgs e)
        {
            await fazerLogin();
        }

        async Task fazerLogin()
        {
            try
            {

                await servico.autenticacaoUsuario(txtEmail.Text, txtSenhaLogin.Text);
                await DisplayAlert("PARABÉNS!", "LOGIN FEITO COM SUCESSO", "OK");
                //await Navigation.PushModalAsync(new PrincipalPage());
                await Navigation.PopPopupAsync();

            }
            catch (Exception ex)
            {
                await DisplayAlert("ERRO!", ex.Message, "OK");
                Console.WriteLine("Erro (Login)", ex.Message);
            }

        }

    }
}